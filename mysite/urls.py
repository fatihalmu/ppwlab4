from django.urls import path
from . import views

urlpatterns = [
     path('', views.homepage, name='homepage'),
     path('about/', views.about, name='about'),
     path('cv/', views.cv, name='cv'),
     path('portofolio/', views.portofolio, name='portofolio'),
     path('rekomendasi/', views.rekomendasi, name='rekomendasi'),
     path('bukutamu/', views.bukutamu, name='bukutamu'),
     path('agendaku/', views.agendaforms, name='agendaku'),
     path('agendaku/hapus/',views.hapusagenda, name ='hapus'),

     
     
               ]
