from django import forms
from .models import agenda

class agendaku(forms.ModelForm):
	class Meta:
		model = agenda

		fields = ['name','location','date','time','category']
		
		widgets ={
		'name': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'Isi nama'}),
		'location': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'isi tempat'}),
		'date': forms.DateInput(attrs={'class': 'form-control','type':'date'}),
		'time': forms.TimeInput(attrs={'class':'form-control','type':'time'}),
		'category': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'isi kategori'}),
		}

		labels = {
            'name': 'Nama Kegiatan',
            'location': 'Tempat Kegiatan',
            'date': 'Tanggal Kegiatan',
            'time': 'Jam Kegiatan',
            'category': 'Kategori Kegiatan',
        }