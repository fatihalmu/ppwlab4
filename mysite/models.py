from django.db import models
from datetime import datetime, date

# Create your models here.
class agenda(models.Model):
	name = models.CharField(max_length = 100)
	category = models.CharField(max_length = 20)
	location = models.CharField(max_length = 200)
	date = models.DateField()
	time = models.TimeField()
