from django.shortcuts import render
from django.shortcuts import reverse

# Create your views here.
from django.http import HttpResponse,HttpResponseRedirect
from .forms import agendaku
from .models import agenda


def homepage(request):
	return render(request, 'homepage.html')

def about(request):
	return render(request, 'about.html')

def agendaforms(request):
	daftaragenda = agenda.objects.all()

	if request.method == "POST":
		form = agendaku(request.POST)
		if form.is_valid():
			agendabaru = form.save()
			agendabaru.save()
			return HttpResponseRedirect(reverse('agendaku'))
		else:
			pass
	else:
		form = agendaku()
		return render(request, 'agendaku.html', {'form': form,'daftaragenda':daftaragenda})
	
def hapusagenda(request):
	if request.method == "POST":
		form = agendaku(request.POST)
		if form.is_valid():
			agendabaru = form.save()
			agendabaru.save()
			return HttpResponseRedirect(reverse('agendaku'))
	form = agendaku()
	daftaragenda = agenda.objects.all().delete()
	return render(request, 'agendaku.html', {'form': form, 'events': daftaragenda})

def cv(request):
	return render(request, 'cv.html')

def rekomendasi(request):
	return render(request, 'rekomendasi.html')

def portofolio(request):
	return render(request, 'portofolio.html')

def bukutamu(request):
	return render(request, 'bukutamu.html')
	